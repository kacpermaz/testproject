//
//  TestProjectTests.swift
//  TestProjectTests
//
//  Created by mazurkk3 on 02/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import XCTest
@testable import TestProject

class TestProjectTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testNumberOfSections() {
        let promise = expectation(description: "Check if number of sections is calculated correctly")
        
        let listViewModel = TPListViewModel()
        
        listViewModel.didReloadModelClosure = {
            XCTAssertEqual(listViewModel.numberOfSections, 5)
            promise.fulfill()
        }
        listViewModel.downloadData(usingMockService: true)
        
        wait(for: [promise], timeout: 10.0)
    }
    
    func testNumberOfParsedDocuments() {
        let promise = expectation(description: "Check if number of mocked documents is correct")
        
        let listViewModel = TPListViewModel()
        
        listViewModel.didReloadModelClosure = {
            XCTAssertEqual(listViewModel.numberOfDocuments, 57)
            promise.fulfill()
        }
        listViewModel.downloadData(usingMockService: true)
        
        wait(for: [promise], timeout: 10.0)
    }
    
    func testIfReloadClosureIsCalled() {
        let promise = expectation(description: "Model closure should be called after data update")

        let listViewModel = TPListViewModel()
        
        listViewModel.didReloadModelClosure = {
            promise.fulfill()
        }
        listViewModel.downloadData(usingMockService: true)
       
        wait(for: [promise], timeout: 10.0)
    }
    
    func testIfDocumentModelChanged() {
        let promise = expectation(description: "Model closure should be called after data update")
        
        let fakeModel = TPDocumentModel(category: "Animals", thumbnailImage: "Morbi ipsum est",
                                        identificator: 15, title: "Morbi ipsum est")
        let documentViewModel = TPDocumentViewModel(model: fakeModel)
        
        documentViewModel.modelDidChangeClosure = {
            promise.fulfill()
        }
        
        documentViewModel.downloadAdditionalDataIfNeeded(usingMockService: true)
        
        
        wait(for: [promise], timeout: 10.0)
    }
    
    func testDocumentsService() {
        let promise = expectation(description: "Download data from documents service and expected type from response")
        
        let documentsService = TPDocumentsService(requestData: nil)
        documentsService.request { result in
            switch result {
            case .success(let result):
                if result is [TPDocumentModel] {
                    promise.fulfill()
                } else {
                    XCTFail("Unexpected type in response")
                }
            case .failure(let error):
                XCTFail(error)
            }
        }
        wait(for: [promise], timeout: 10.0)
    }
    
    func testDocumentDetailsService() {
        let promise = expectation(description: "Download specific documents data and check returned id")
        
        let expectedId = Int(arc4random_uniform(195)) // documentId is an integer between 0 and 195
        
        let requestData = TPDocumentData(id: expectedId)
        let detailsService = TPDocumentDetailsService(requestData: requestData)
        
        detailsService.request { result in
            switch result {
            case .success(let result):
                guard let expected = result as? TPDocumentModel else {
                    XCTFail("Unexpected type in response")
                    return
                }
                
                if expected.identificator != expectedId {
                    XCTFail("Unexpected document id in response")
                }
                
                promise.fulfill()

            case .failure(let error):
                XCTFail(error)
            }
        }
        wait(for: [promise], timeout: 10.0)
    }
    
    func testHTMLTagsRemovalPerformance() {
        let fakeModel = TPDocumentModel(category: "Animals", thumbnailImage: "Morbi ipsum est",
                                        identificator: 15, title: "Morbi ipsum est")
        
        let documentViewModel = TPDocumentViewModel(model: fakeModel)

        documentViewModel.modelDidChangeClosure = {
            self.measure {
                20.times(f: { _ in
                    let _ = documentViewModel.text
                })
            }
        }
        
        documentViewModel.downloadAdditionalDataIfNeeded(usingMockService: true)
    }
}
