//
//  TPAlertViews.swift
//  TestProject
//
//  Created by mazurkk3 on 04/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import UIKit

extension UIAlertController {
    static func showAlert(with vc: UIViewController?, title: String, description: String, completion: (() -> Void)? = nil) {
        guard let vc = vc else {
            return
        }
        let alertVC = UIAlertController(title: title, message: description, preferredStyle: .alert)
        let action = UIAlertAction(title: Constants.Buttons.ok, style: .default) { action in
            completion?()
        }
        alertVC.addAction(action)
        vc.present(alertVC, animated: true, completion: nil)
    }
}
