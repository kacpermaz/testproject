//
//  UIColor+TP.swift
//  TestProject
//
//  Created by mazurkk3 on 03/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import UIKit

extension UIColor {
    class func tpBlueColor() -> UIColor {
        return UIColor(red: 59.0/255.0, green: 80.0/255.0, blue: 106.0/255.0, alpha: 1.0)
    }
    
    class func tpSectionHeaderColor() -> UIColor {
        return UIColor(red: 28.0/255.0, green: 38.0/255.0, blue: 64.0/255.0, alpha: 1.0)
    }
    
    class func tpFontColor() -> UIColor {
        return UIColor(red: 11.0/255.0, green: 20.0/255.0, blue: 42.0/255.0, alpha: 1.0)
    }
    
    class func tpGreenColor() -> UIColor {
        return UIColor(red: 95.0/255.0, green: 192.0/255.0, blue: 189.0/255.0, alpha: 1.0)
    }
}
