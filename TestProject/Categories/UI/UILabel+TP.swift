//
//  UILabel+TP.swift
//  TestProject
//
//  Created by mazurkk3 on 03/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import UIKit

extension UILabel {
    convenience init(font: UIFont, color: UIColor, numberOfLines: Int = 1,
                     text: String? = nil, textAlignment: NSTextAlignment = .natural) {
        self.init(frame: .zero)
        self.textColor = color
        self.font = font
        self.numberOfLines = numberOfLines
        self.text = text
        self.textAlignment = textAlignment
    }
}
