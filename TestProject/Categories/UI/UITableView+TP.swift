//
//  UITableView+TP.swift
//  TestProject
//
//  Created by mazurkk3 on 02/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import UIKit

extension UITableView {
    
    func dequeueReusableCell<T: UITableViewCell>(_ type: T.Type) -> T {
        return self.dequeueReusableCell(withIdentifier: NSStringFromClass(type)) as! T
    }
    
    func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>(_ type: T.Type) -> T {
        return self.dequeueReusableHeaderFooterView(withIdentifier: NSStringFromClass(type)) as! T
    }
    
    func registerCell<T: UITableViewCell>(_ type: T.Type) {
        self.register(type, forCellReuseIdentifier: NSStringFromClass(type))
    }
    
    func registerHeaderFooterView<T: UITableViewHeaderFooterView>(_ type: T.Type) {
        self.register(type, forHeaderFooterViewReuseIdentifier: NSStringFromClass(type))
    }
}
