//
//  UIView+TPLayout.swift
//  TestProject
//
//  Created by mazurkk3 on 02/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import UIKit

struct TPMargin {
    static let increased = CGPoint(x: 32, y: 32)
    static let standard = CGPoint(x: 16, y: 16)
    static let reduced = CGPoint(x: 12, y: 12)
    static let minimal = CGPoint(x: 8, y: 8)
    
    static let defaultHorizontalSeparator: CGFloat = 15.0
}

struct TPSpacing {
    static let standard = CGPoint(x: 8, y: 8)
    static let reduced = CGPoint(x: 4, y: 4)
    static let minimal = CGPoint(x: 2, y: 2)
}

struct TPAxisConstaints {
    let x: NSLayoutConstraint?
    let y: NSLayoutConstraint?
}

struct MultiMarginResult {
    let top: NSLayoutConstraint?
    let left: NSLayoutConstraint?
    let right: NSLayoutConstraint?
    let bottom: NSLayoutConstraint?
}

fileprivate extension NSLayoutConstraint {
    func activated(withPriority priority: UILayoutPriority) -> NSLayoutConstraint {
        self.priority = priority
        self.isActive = true
        return self
    }
}

extension UIView {
    
    @discardableResult func pinCenterX(offset: CGFloat = 0,
                                       priority: UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        return centerXAnchor.constraint(equalTo: superview!.centerXAnchor, constant: offset)
            .activated(withPriority: priority)
    }
    
    @discardableResult func pinCenterY(offset: CGFloat = 0,
                                       priority: UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        return centerYAnchor.constraint(equalTo: superview!.centerYAnchor, constant: offset)
            .activated(withPriority: priority)
    }
    
    @discardableResult func pinCenter(xOffset: CGFloat = 0, yOffset: CGFloat = 0,
                                      priority: UILayoutPriority = UILayoutPriority.required) -> TPAxisConstaints {
        return TPAxisConstaints(x: self.pinCenterX(offset: xOffset, priority: priority),
                                y: self.pinCenterY(offset: yOffset, priority: priority))
    }
    
    @discardableResult func pinLeft(leftMargin: CGFloat = 0,
                                    priority: UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        return leftAnchor.constraint(equalTo: superview!.leftAnchor, constant: leftMargin)
            .activated(withPriority: priority)
    }
    
    @discardableResult func pinRight(rightMargin: CGFloat = 0,
                                     priority: UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        return rightAnchor.constraint(equalTo: superview!.rightAnchor, constant: -rightMargin)
            .activated(withPriority: priority)
    }
    
    @discardableResult func pinTop(topMargin: CGFloat = 0,
                                   priority: UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        return topAnchor.constraint(equalTo: superview!.topAnchor, constant: topMargin)
            .activated(withPriority: priority)
    }
    
    @discardableResult func pinBottom(bottomMargin: CGFloat = 0,
                                      priority: UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint? {
        return bottomAnchor.constraint(equalTo: superview!.bottomAnchor, constant: -bottomMargin)
            .activated(withPriority: priority)
    }
    
    @discardableResult func pinTopLeft(topMargin: CGFloat = 0, leftMargin: CGFloat = 0,
                                       priority: UILayoutPriority = UILayoutPriority.required) -> MultiMarginResult {
        return MultiMarginResult(top: self.pinTop(topMargin: topMargin, priority: priority),
                                 left: self.pinLeft(leftMargin: leftMargin, priority: priority),
                                 right: nil, bottom: nil)
    }
    
    @discardableResult func pinBottomRight(rightMargin: CGFloat = 0, bottomMargin: CGFloat = 0,
                                           priority: UILayoutPriority = UILayoutPriority.required) -> MultiMarginResult {
        return MultiMarginResult(top: nil, left: nil,
                                 right: self.pinRight(rightMargin: rightMargin, priority: priority),
                                 bottom: self.pinBottom(bottomMargin: bottomMargin, priority: priority))
    }
    
    @discardableResult func pinToHorizontalMargins(leftMargin: CGFloat = 0, rightMargin: CGFloat = 0,
                                                   priority: UILayoutPriority = UILayoutPriority.required) -> MultiMarginResult {
        return MultiMarginResult(top: nil,
                                 left: self.pinLeft(leftMargin: leftMargin, priority: priority),
                                 right: self.pinRight(rightMargin: rightMargin, priority: priority),
                                 bottom: nil)
    }
    
    @discardableResult func pinToVerticalMargins(topMargin: CGFloat = 0, bottomMargin: CGFloat = 0,
                                                 priority: UILayoutPriority = UILayoutPriority.required) -> MultiMarginResult {
        return MultiMarginResult(top: self.pinTop(topMargin: topMargin, priority: priority),
                                 left: nil, right: nil,
                                 bottom: self.pinBottom(bottomMargin: bottomMargin, priority: priority))
    }
    
    @discardableResult func fillSuperview(topMargin: CGFloat = 0, leftMargin: CGFloat = 0, rightMargin: CGFloat = 0, bottomMargin: CGFloat = 0,
                                          priority: UILayoutPriority = UILayoutPriority.required) -> MultiMarginResult {
        return MultiMarginResult(top: self.pinTop(topMargin: topMargin, priority: priority),
                                 left: self.pinLeft(leftMargin: leftMargin, priority: priority),
                                 right: self.pinRight(rightMargin: rightMargin, priority: priority),
                                 bottom: self.pinBottom(bottomMargin: bottomMargin, priority: priority))
    }
    
    @discardableResult func pinLeftOf(view: UIView, margin: CGFloat = 0,
                                      priority: UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        return rightAnchor.constraint(equalTo: view.leftAnchor, constant: -margin)
            .activated(withPriority: priority)
    }
    
    @discardableResult func pinRightOf(view: UIView, margin: CGFloat = 0,
                                       priority: UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        return leftAnchor.constraint(equalTo: view.rightAnchor, constant: margin)
            .activated(withPriority: priority)
    }
    
    @discardableResult func pinAbove(view: UIView, margin: CGFloat = 0,
                                     priority: UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        return bottomAnchor.constraint(equalTo: view.topAnchor, constant: -margin)
            .activated(withPriority: priority)
    }
    
    @discardableResult func pinBelow(view: UIView, margin: CGFloat = 0,
                                     priority: UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        return topAnchor.constraint(equalTo: view.bottomAnchor, constant: margin)
            .activated(withPriority: priority)
    }
    
    @discardableResult func pinCenterX(to view: UIView, offset: CGFloat = 0,
                                       priority: UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        return centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: offset)
            .activated(withPriority: priority)
    }
    
    @discardableResult func pinCenterY(to view: UIView, offset: CGFloat = 0,
                                       priority: UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        return centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: offset)
            .activated(withPriority: priority)
    }
    
    @discardableResult func pinTop(to view: UIView, offset: CGFloat = 0,
                                   priority: UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        return topAnchor.constraint(equalTo: view.topAnchor, constant: offset)
            .activated(withPriority: priority)
    }
    
    @discardableResult func pinBottom(to view: UIView, offset: CGFloat = 0,
                                      priority: UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        return bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: offset)
            .activated(withPriority: priority)
    }
    
    @discardableResult func pinHeight(heightRatio: CGFloat = 1.0,
                                      priority: UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        return heightAnchor.constraint(equalTo: superview!.heightAnchor, multiplier: heightRatio)
            .activated(withPriority: priority)
    }
    
    @discardableResult func pinWidth(widthRatio: CGFloat = 1.0,
                                     priority: UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        return widthAnchor.constraint(equalTo: superview!.widthAnchor, multiplier: widthRatio)
            .activated(withPriority: priority)
    }
    
    @discardableResult func pinCenter(to view: UIView, xOffset: CGFloat = 0, yOffset: CGFloat = 0,
                                      priority: UILayoutPriority = UILayoutPriority.required) -> TPAxisConstaints {
        return TPAxisConstaints(x: self.pinCenterX(to: view, offset: xOffset, priority: priority),
                                y: self.pinCenterY(to: view, offset: yOffset, priority: priority))
    }
    
    @discardableResult func pinSize(width: CGFloat? = nil, height: CGFloat? = nil,
                                    priority: UILayoutPriority = UILayoutPriority.required) -> TPAxisConstaints {
        var widthConstraint: NSLayoutConstraint? = nil
        var heightConstraint: NSLayoutConstraint? = nil
        
        if let width = width {
            widthConstraint = self.widthAnchor.constraint(equalToConstant: width).activated(withPriority: priority)
        }
        
        if let height = height {
            heightConstraint = self.heightAnchor.constraint(equalToConstant: height).activated(withPriority: priority)
        }
        
        return TPAxisConstaints(x: widthConstraint, y: heightConstraint)
    }
    
    func addSubviewsAndDisableAutoresizingMaskTranslation(_ subviews: [UIView]) {
        subviews.forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
        subviews.forEach { self.addSubview($0) }
    }
}

