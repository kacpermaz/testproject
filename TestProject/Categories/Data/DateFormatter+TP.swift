//
//  DateFormatter+TP.swift
//  TestProject
//
//  Created by mazurkk3 on 05/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import Foundation

extension DateFormatter {
    func documentStringFormatToDate(stringDate: String) -> Date? {
        self.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSS"
        return self.date(from: stringDate)
    }
    
    func dateToPrettyStringFormat(date: Date) -> String? {
        self.dateFormat = "MMM d, yyyy HH:mm:ss"
        return self.string(from: date)
    }
}
