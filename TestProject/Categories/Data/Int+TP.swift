//
//  Int+TP.swift
//  TestProject
//
//  Created by mazurkk3 on 04/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import Foundation
extension Int {
    func times(f: (_ index: Int) -> ()) {
        if self > 0 {
            for index in 0..<self {
                f(index)
            }
        }
    }
}

