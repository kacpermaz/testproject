//
//  TPDocumentModel.swift
//  TestProject
//
//  Created by mazurkk3 on 02/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import Foundation
import ObjectMapper

enum TPDocumentImageType {
    case Thumbnail
    case Header
    
    var cache : TPImageCache {
        switch self {
        case .Thumbnail:
            return TPImageCache.thumbnails
        case .Header:
            return TPImageCache.headers
        }
    }
}

struct TPDocumentModel: ImmutableMappable, TPRequestData {
    var category: String
    var thumbnailImage: String
    var identificator: Int
    var title: String
    var headerImg : String?
    var text : String?
    var created : Date?
    var author : String?
    
    init(map: Map) throws {
        category = try map.value("category")
        thumbnailImage = try map.value("thumbnailImg")
        identificator = try map.value("id")
        title = try map.value("title")
        headerImg = try? map.value("headerImg")
        text = try? map.value("text")
        created = try? map.value("created", using: CustomDateTransform())
        author = try? map.value("author")
    }
    
    init(category: String, thumbnailImage: String, identificator: Int, title: String,
         headerImg: String? = nil, text: String? = nil, created: Date? = nil, author: String? = nil) {
        self.category = category
        self.thumbnailImage = thumbnailImage
        self.identificator = identificator
        self.title = title
        self.headerImg = headerImg
        self.text = text
        self.created = created
        self.author = author
    }
    
    func mapping(map: Map) { /* not serializable */ }
    
}

fileprivate class CustomDateTransform: TransformType {
    
    public typealias Object = Date
    public typealias JSON = [String]
    
    public init() {}
    
    func transformFromJSON(_ value: Any?) -> Object? {
        guard let stringDate = value as? String else {
            return nil
        }
        
        let dateFormatter = DateFormatter()
        return dateFormatter.documentStringFormatToDate(stringDate: stringDate)
    }
    
    func transformToJSON(_ value: Date?) -> JSON? {
         /* not serializable */
        return nil
    }
}
