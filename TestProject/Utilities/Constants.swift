//
//  Constants.swift
//  TestProject
//
//  Created by mazurkk3 on 02/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import Foundation

enum Constants {
    enum HTTP {
        static let documentsRequestStringURL = "http://localhost:8080/api/v1/documents"
    }
    
    enum Title {
        static let documentsVC = "Documents"
        static let detailVC = "Details"
    }
    
    enum DetailView {
        static let category = "Category:"
        static let created = "Created:"
        static let author = "Author:"
    }
    
    enum Errors {
        static let wrongFormatReturned = "Wrong format returned"
        static let keyNotFound = "Could not find key in Json response. Key: "
        static let map = "Could not map to model"
        static let noDataReturned = "No data returned"
        static let parse = "Could not parse the data"
    }
    
    enum Alerts {
        static let dataDownloading = "Data downloading error"
    }
    
    enum PlaceholderStrings {
        static let unknown = "Unknown"
    }
    
    enum Buttons {
        static let ok = "Ok"
    }
    
    enum RefreshControl {
        static let pullToRefresh = "Pull to refresh"
    }
}
