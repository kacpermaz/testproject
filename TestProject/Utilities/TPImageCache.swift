//
//  TPImageCache.swift
//  TestProject
//
//  Created by mazurkk3 on 03/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import UIKit

class TPImageCache {

    private let cache = NSCache<NSString, UIImage>()
    
    func imageFor(key: String) -> UIImage? {
        guard let imageInCache = self.cache.object(forKey: key as NSString) else {
            return nil
        }
        return imageInCache
    }
    
    func setImage(image: UIImage, key: String) {
        self.cache.setObject(image, forKey: key as NSString)
    }
}

// MARK: Global Cache

extension TPImageCache {
    static let thumbnails = TPImageCache()
    static let headers = TPImageCache()
    
    static func clearGlobalCaches() {
        [thumbnails, headers].forEach {
            $0.cache.removeAllObjects()
        }
    }
}
