//
//  TPListCell.swift
//  TestProject
//
//  Created by mazurkk3 on 02/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import UIKit
import AlamofireImage

class TPListCell: UITableViewCell {
    
    private let titleLabel = UILabel(font: .systemFont(ofSize: 16, weight: .regular),
                                     color: UIColor.tpFontColor(), numberOfLines: 0)
    
    private let iconView = TPIconView(frame: .zero)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        accessoryType = .disclosureIndicator
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configured(withViewModel viewModel: TPDocumentViewModel) -> Self {
        titleLabel.text = viewModel.title
        
        iconView.image = nil
        
        tag = viewModel.identificator
        
        iconView.activityIndicator.startAnimating()
        viewModel.image(type: .Thumbnail, completion: { [weak self] image, error in
            if viewModel.identificator == self?.tag {
                self?.iconView.activityIndicator.stopAnimating()
                self?.iconView.image = image 
            }
        })
        
        return self
    }
    
    private func setupViews() {
        contentView.backgroundColor = .white
        contentView.addSubviewsAndDisableAutoresizingMaskTranslation([titleLabel, iconView])
        
        iconView.pinTopLeft(topMargin: TPMargin.minimal.y, leftMargin: TPMargin.reduced.x)
        iconView.pinSize(width: 50, height: 50)
        iconView.pinBottom(bottomMargin: TPMargin.minimal.y)
        
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.pinRightOf(view: iconView, margin: TPMargin.standard.x)
        titleLabel.pinCenterY(to: iconView)
        titleLabel.pinRight(rightMargin: TPMargin.reduced.x)
        titleLabel.pinToVerticalMargins(topMargin: TPMargin.minimal.y, bottomMargin: TPMargin.minimal.y)
    }
}

