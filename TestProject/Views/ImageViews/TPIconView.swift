//
//  TPIconView.swift
//  TestProject
//
//  Created by mazurkk3 on 03/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import UIKit

class TPIconView: UIImageView {
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        customizeView()
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func customizeView() {
        self.layer.cornerRadius = 5.0
        
        self.clipsToBounds = true
    }
    
    private func setupViews() {
        addSubviewsAndDisableAutoresizingMaskTranslation([activityIndicator])
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.pinCenter()
    }
}
