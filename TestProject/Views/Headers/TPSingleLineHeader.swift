//
//  TPSingleLineHeader.swift
//  TestProject
//
//  Created by mazurkk3 on 02/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import UIKit

class TPSingleLineHeader: UITableViewHeaderFooterView {
    
    private let titleLabel = UILabel(font: .systemFont(ofSize: 19), color: .white, numberOfLines: 1)
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        contentView.backgroundColor = UIColor.tpSectionHeaderColor()
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        contentView.addSubviewsAndDisableAutoresizingMaskTranslation([titleLabel])
        
        self.titleLabel.pinToHorizontalMargins(leftMargin: TPMargin.standard.x,
                                               rightMargin: TPMargin.standard.x)
        
        self.titleLabel.pinToVerticalMargins(topMargin: TPMargin.reduced.y,
                                             bottomMargin: TPMargin.reduced.y)
    }
    
    func configured(with title: String) -> Self {
        titleLabel.text = title
        
        return self
    }
}
