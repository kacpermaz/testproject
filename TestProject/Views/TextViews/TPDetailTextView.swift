//
//  TPDetailTextView.swift
//  TestProject
//
//  Created by mazurkk3 on 03/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import UIKit

class TPDetailTextView: UITextView {
    
    init() {
        super.init(frame: .zero, textContainer: nil)
        
        customizeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func customizeView() {
        isEditable = false
        isScrollEnabled = false
        textAlignment = .justified
        font = UIFont.systemFont(ofSize: 14, weight: .regular)
        textColor = UIColor.tpFontColor()
    }
}
