//
//  TPFakeDocumentsService.swift
//  TestProject
//
//  Created by mazurkk3 on 04/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import Foundation

class TPFakeDocumentsService: TPHTTPService {
    override func request(_ completion: @escaping TPHTTPResultBlock) {
        guard let path = Bundle.main.path(forResource: "fakeDocumentsData", ofType: "json"),
            let jsonData = NSData(contentsOfFile: path) as Data?,
            let parsingResult = try? JSONSerialization.jsonObject(with: jsonData, options: []),
            let json = parsingResult as? [String:Any],
            let jsonArray = json["documents"] as? [[String: Any]] else {
                completion(.failure(Constants.Errors.parse))
                return
        }
        let result = jsonArray.flatMap { try? TPDocumentModel(JSON: $0) }
        completion(.success(result))
    }
}
