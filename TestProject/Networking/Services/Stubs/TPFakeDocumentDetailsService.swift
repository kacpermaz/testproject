//
//  TPFakeDocumentDetailsService.swift
//  TestProject
//
//  Created by mazurkk3 on 04/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import Foundation

class TPFakeDocumentDetailsService: TPHTTPService {
    override func request(_ completion: @escaping TPHTTPResultBlock) {
        guard let path = Bundle.main.path(forResource: "fakeDocumentDetailsData", ofType: "json"),
            let jsonData = NSData(contentsOfFile: path) as Data?,
            let parsingResult = try? JSONSerialization.jsonObject(with: jsonData, options: []),
            let json = parsingResult as? [String:Any] else {
                completion(.failure(Constants.Errors.parse))
                return
        }
        if let result = try? TPDocumentModel(JSONObject: json) {
            completion(.success(result))
        } else {
            completion(.failure(Constants.Errors.parse))
        }
    }
}
