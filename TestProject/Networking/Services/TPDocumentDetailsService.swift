//
//  TPDocumentDetailsService.swift
//  TestProject
//
//  Created by mazurkk3 on 03/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import Foundation

class TPDocumentDetailsService: TPHTTPService {
    
    override func createRequest() -> URLRequest? {
        guard let requestData = requestData as? TPDocumentData else {
            return nil
        }
        
        return TPHTTPRequestFactory.documentDetailsRequest(with: requestData)
    }
    
    override func processResponse(fromParsingResult result: TPHTTPDataParseResult,
                                  completion: @escaping TPHTTPResultBlock) {
        switch result {
        case .object(let object):
            if let document = try? TPDocumentModel(JSONObject: object) {
                completion(TPHTTPResult.success(document))
            } else {
                completion(TPHTTPResult.failure(Constants.Errors.map))
            }
        default:
            completion(TPHTTPResult.failure(Constants.Errors.wrongFormatReturned))
        }
    }
}
