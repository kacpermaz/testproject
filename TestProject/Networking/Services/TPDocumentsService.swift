//
//  TPDocumentsService.swift
//  TestProject
//
//  Created by mazurkk3 on 02/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import Foundation

class TPDocumentsService: TPHTTPService {
    
    private let documentsKey = "documents"
    
    override func createRequest() -> URLRequest? {
        return TPHTTPRequestFactory.documentsRequest()
    }
    
    override func processResponse(fromParsingResult result: TPHTTPDataParseResult,
                                  completion: @escaping TPHTTPResultBlock) {
        switch result {
        case .object(let object):
            if let result = object[documentsKey].flatMap({ $0 as? TPJsonArray }) {
                let documents = result.flatMap { try? TPDocumentModel(JSON: $0) }
                completion(TPHTTPResult.success(documents))
            } else {
                completion(TPHTTPResult.failure(Constants.Errors.keyNotFound + documentsKey))
            }
        default:
            completion(TPHTTPResult.failure(Constants.Errors.wrongFormatReturned))
        }
    }
    
}
