//
//  TPRequestData.swift
//  TestProject
//
//  Created by mazurkk3 on 03/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import Foundation

protocol TPRequestData {}

struct TPDocumentData: TPRequestData {
    let id : Int
}
