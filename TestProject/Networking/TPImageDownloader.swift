//
//  TPImageDownloader.swift
//  TestProject
//
//  Created by mazurkk3 on 03/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import Foundation
import AlamofireImage

class TPImageDownloader: ImageDownloader {
    
    init() {
        super.init(configuration: ImageDownloader.defaultURLSessionConfiguration(),
                   downloadPrioritization: .lifo,
                   maximumActiveDownloads: 6,
                   imageCache: nil)
    }
}
