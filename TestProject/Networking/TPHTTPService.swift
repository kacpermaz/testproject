//
//  TPHTTPService.swift
//  TestProject
//
//  Created by mazurkk3 on 02/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import Foundation

class TPHTTPService {
    
    var requestData: TPRequestData?
    
    init(requestData: TPRequestData?) {
        self.requestData = requestData
    }
    
    // override to create the required http request
    func createRequest() -> URLRequest? {
        return nil
    }
    
    // override to process data before returning it the call site
    func processResponse(fromParsingResult result: TPHTTPDataParseResult,
                         completion: @escaping TPHTTPResultBlock) { }
    
    func request(_ completion: @escaping TPHTTPResultBlock) {
        guard let request = self.createRequest() else {
            return
        }
        
        TPHTTPService.newSession().dataTask(with: request, completionHandler: { (data, response, error) in
            DispatchQueue.main.async(execute: {
                if let nsurlError = error {
                    return completion(TPHTTPResult.failure(nsurlError.localizedDescription))
                }
                
                guard let data = data else {
                    return completion(TPHTTPResult.failure(Constants.Errors.noDataReturned))
                }
                
                if let parsedData = TPHTTPService.parse(data: data) {
                    return self.processResponse(fromParsingResult: parsedData, completion: completion)
                }
                
                return completion(TPHTTPResult.failure(Constants.Errors.parse))
            })
        }).resume()
    }
}

extension TPHTTPService {
    
    static private func newSession() -> URLSession {
        return URLSession(configuration: URLSessionConfiguration.default)
    }
    
    static private func parse(data: Data) -> TPHTTPDataParseResult? {
        if let serializedObject = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) {
            
            if let jsonObject = serializedObject as? TPJsonObject {
                return TPHTTPDataParseResult.object(jsonObject)
            } else if let jsonArray = serializedObject as? TPJsonArray {
                return TPHTTPDataParseResult.array(jsonArray)
            }
        }
        
        if let string = String(bytes: data, encoding: String.Encoding.utf8){
            return TPHTTPDataParseResult.string(string)
        }
        
        return nil
    }
}
