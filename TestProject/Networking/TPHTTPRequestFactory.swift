//
//  TPHTTPRequestFactory.swift
//  TestProject
//
//  Created by mazurkk3 on 02/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import Foundation

private struct HeaderKeys {
    static let ContentType = "Content-type"
}

private struct HeaderValues {
    static let Json = "application/json"
}

private struct Methods {
    static let GET = "GET"
    static let POST = "POST"
}

struct TPHTTPRequestFactory {
    static func documentsRequest() -> URLRequest {
        let request = baseRequest()
        
        return request
    }
    
    static func documentDetailsRequest(with data: TPDocumentData) -> URLRequest {
        var request = baseRequest()
        request.url?.appendPathComponent(String(data.id))
        
        return request
    }
    
    private static func baseRequest() -> URLRequest {
        let url = URL(string: Constants.HTTP.documentsRequestStringURL)!
        
        var request = URLRequest(url: url)
        request.httpMethod = Methods.GET
        
        request.setValue(HeaderValues.Json, forHTTPHeaderField: HeaderKeys.ContentType)
        return request
    }
}
