//
//  TPHTTPResult.swift
//  TestProject
//
//  Created by mazurkk3 on 02/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import Foundation

typealias TPHTTPResultBlock =  (_ result: TPHTTPResult<Any>) -> Void

typealias TPJsonObject = [String: AnyObject]
typealias TPJsonArray = [TPJsonObject]

enum TPHTTPDataParseResult {
    case object(TPJsonObject)
    case array(TPJsonArray)
    case string(String)
}

enum TPHTTPResult<T> {
    case success(T)
    case failure(String)
    
    var isSuccess: Bool {
        switch self {
        case .success:
            return true
        case .failure:
            return false
        }
    }
}
