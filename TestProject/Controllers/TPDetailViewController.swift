//
//  TPDetailViewController.swift
//  TestProject
//
//  Created by mazurkk3 on 03/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import UIKit

class TPDetailViewController: UIViewController {
    
    private let detailViewModel : TPDocumentViewModel
    
    private let headerImageView = UIImageView(frame: .zero)
    
    private let titleBackgroundView = UIView(frame: .zero)
    
    private let titleLabel = UILabel(font: .systemFont(ofSize: 20, weight: .heavy),
                                     color: .white, numberOfLines: 0, textAlignment: .center)
    
    private let categoryDescription = UILabel(font: .systemFont(ofSize: 13, weight: .heavy),
                                              color: UIColor.tpFontColor(), text: Constants.DetailView.category)
    
    private let categoryLabel = UILabel(font: .systemFont(ofSize: 13, weight: .light),
                                        color: UIColor.tpFontColor())
    
    private let createdDescription = UILabel(font: .systemFont(ofSize: 13, weight: .heavy),
                                             color: UIColor.tpFontColor(), text: Constants.DetailView.created)
    
    private let createdDateLabel = UILabel(font: .systemFont(ofSize: 13, weight: .light),
                                           color: UIColor.tpFontColor(), text : Constants.PlaceholderStrings.unknown)
    
    private let authorDescription = UILabel(font: .systemFont(ofSize: 13, weight: .heavy),
                                            color: UIColor.tpFontColor(), text: Constants.DetailView.author)
    
    private let authorLabel = UILabel(font: .systemFont(ofSize: 13, weight: .light),
                                      color: UIColor.tpFontColor())
    
    private let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)

    private let scrollView = UIScrollView(frame: .zero)
    
    private let textView = TPDetailTextView()
    
    init(viewModel: TPDocumentViewModel) {
        self.detailViewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
        
        title = Constants.Title.detailVC
        
        activityIndicator.hidesWhenStopped = true
        
        headerImageView.contentMode = .scaleAspectFill
        headerImageView.clipsToBounds = true
        
        titleBackgroundView.backgroundColor = UIColor.tpSectionHeaderColor()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        updateViews()
        
        configureViewModel()
    }
    
    private func configureViewModel() {
        detailViewModel.downloadAdditionalDataIfNeeded()
        detailViewModel.modelDidChangeClosure = { [weak self] in
            self?.updateViews()
        }
    }
    
    private func updateViews() {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
        }
        detailViewModel.image(type: .Header, completion: { [weak self] image, error in
            DispatchQueue.main.async {
                self?.activityIndicator.stopAnimating()
                self?.headerImageView.image = image
            }
        })
        
        detailViewModel.showAlertClosure = { [weak self] reason, title in
            UIAlertController.showAlert(with: self, title: title, description: reason)
        }
        
        textView.text = detailViewModel.text
        titleLabel.text = detailViewModel.title
        categoryLabel.text = detailViewModel.category
        authorLabel.text = detailViewModel.author ?? Constants.PlaceholderStrings.unknown

        if let date = detailViewModel.createdDate {
            let dateFormatter = DateFormatter()
            createdDateLabel.text = dateFormatter.dateToPrettyStringFormat(date: date)
        }
    }
    
    private func setupViews() {
        view.backgroundColor = .white
        self.view.addSubviewsAndDisableAutoresizingMaskTranslation([scrollView])

        scrollView.fillSuperview()
        
        let views = [headerImageView, activityIndicator, titleBackgroundView, categoryDescription,
                     createdDescription, categoryLabel, createdDateLabel,
                     authorDescription, authorLabel, textView]
        
        scrollView.addSubviewsAndDisableAutoresizingMaskTranslation(views)
        
        headerImageView.pinCenterX()
        headerImageView.pinTop()
        headerImageView.pinToHorizontalMargins()
        headerImageView.pinSize(height: 110)
        
        activityIndicator.pinCenter(to: headerImageView)
        activityIndicator.pinSize(width: 25, height: 25)
        
        titleBackgroundView.addSubviewsAndDisableAutoresizingMaskTranslation([titleLabel])
        titleBackgroundView.pinBelow(view: headerImageView)
        titleBackgroundView.pinToHorizontalMargins()
        
        titleLabel.fillSuperview(topMargin: TPMargin.reduced.y, leftMargin: TPMargin.reduced.y,
                                 rightMargin: TPMargin.reduced.x, bottomMargin: TPMargin.reduced.y)
        
        categoryDescription.pinBelow(view: titleBackgroundView, margin: TPMargin.standard.y)
        categoryDescription.pinLeft(leftMargin: TPMargin.reduced.x)
        
        categoryLabel.pinRightOf(view: categoryDescription, margin: TPMargin.minimal.x)
        categoryLabel.pinCenterY(to: categoryDescription)
        
        authorDescription.pinBelow(view: categoryLabel, margin: TPMargin.standard.y)
        authorDescription.pinLeft(leftMargin: TPMargin.reduced.x)

        authorLabel.pinRightOf(view: authorDescription, margin: TPMargin.minimal.x)
        authorLabel.pinCenterY(to: authorDescription)
        
        createdDescription.pinBelow(view: authorLabel, margin: TPMargin.standard.y)
        createdDescription.pinLeft(leftMargin: TPMargin.reduced.x)
        
        createdDateLabel.pinRightOf(view: createdDescription, margin: TPMargin.minimal.x)
        createdDateLabel.pinCenterY(to: createdDescription)
        
        textView.pinBelow(view: createdDateLabel, margin: TPMargin.standard.y)
        textView.pinToHorizontalMargins(leftMargin: TPMargin.minimal.x, rightMargin: TPMargin.minimal.x)
        textView.pinBottom()
    }
}
