//
//  TPMainTableViewController.swift
//  TestProject
//
//  Created by mazurkk3 on 02/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import UIKit

class TPMainTableViewController: UIViewController {

    private let tableView = UITableView(frame: .zero)
    
    private let refreshControl = UIRefreshControl()
    
    private let documentsViewModel = TPListViewModel()
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        title = Constants.Title.documentsVC
        
        registerCells()
        registerHeadersFooters()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViewModel()
        configureRefreshControl()
        setupViews()
    }
    
    @objc func reloadData() {
        documentsViewModel.downloadData()
    }
    
    private func configureViewModel() {
        documentsViewModel.didReloadModelClosure = { [weak self] in
            self?.refreshControl.endRefreshing()
            self?.tableView.reloadData()
            TPImageCache.clearGlobalCaches()
        }
        
        documentsViewModel.showAlertClosure = { [weak self] reason, title in
            UIAlertController.showAlert(with: self, title: title, description: reason, completion: {
                self?.refreshControl.endRefreshing()
            })
        }
        
        reloadData()
    }
    
    private func configureRefreshControl() {
        refreshControl.attributedTitle = NSAttributedString(string: Constants.RefreshControl.pullToRefresh)
        refreshControl.addTarget(self, action: #selector(reloadData), for: .valueChanged)
    }
    
    private func setupViews() {
        view.addSubviewsAndDisableAutoresizingMaskTranslation([tableView])
        view.backgroundColor = UIColor.white

        tableView.backgroundView = refreshControl
        tableView.fillSuperview()
    }
    
    private func registerHeadersFooters() {
        tableView.registerHeaderFooterView(TPSingleLineHeader.self)
    }
    
    private func registerCells() {
        tableView.registerCell(TPListCell.self)
    }
}

// MARK: TableView Delegate and DataSource methods
extension TPMainTableViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return documentsViewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return documentsViewModel.numberOfCells(forSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellViewModel = documentsViewModel.getCellModel(at: indexPath)
        
        return tableView.dequeueReusableCell(TPListCell.self)
            .configured(withViewModel: cellViewModel)
    }
        
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let titleOfSection = documentsViewModel.title(for: section)
        
        return tableView.dequeueReusableHeaderFooterView(TPSingleLineHeader.self)
            .configured(with: titleOfSection)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    
        let cellViewModel = documentsViewModel.getCellModel(at: indexPath)
        let detailViewController = TPDetailViewController(viewModel: cellViewModel)
        
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
}

