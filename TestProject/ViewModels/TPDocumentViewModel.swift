//
//  TPDocumentViewModel.swift
//  TestProject
//
//  Created by mazurkk3 on 03/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

import UIKit

class TPDocumentViewModel {
    
    var showAlertClosure : TPAlertClosure?
    var modelDidChangeClosure : (() -> Void)?
    
    private let imageDownloader = TPImageDownloader()
    
    private var isAdditionalDataDownloaded = false
    
    private var documentModel : TPDocumentModel {
        didSet {
            modelDidChangeClosure?()
        }
    }
    
    var thumbnailURL : String {
        return documentModel.thumbnailImage
    }
    
    var identificator : Int {
        return documentModel.identificator
    }
    
    var title : String {
        return documentModel.title
    }
    
    var category : String {
        return documentModel.category
    }
    
    var headerImageURL : String? {
        return documentModel.headerImg
    }
    
    var createdDate : Date? {
        return documentModel.created
    }
    
    var text : String? {
        guard let text = documentModel.text else {
            return nil
        }
        return removeHTMLTags(from: text)
    }
    
    var author : String? {
        return documentModel.author
    }
    
    init(model: TPDocumentModel) {
        self.documentModel = model
    }
}

// MARK: Public methods

extension TPDocumentViewModel {
    
    func image(type: TPDocumentImageType, completion: @escaping (UIImage?, Error?) -> Void) {
        let cacheKey = String(identificator)
        
        if let cacheImage = type.cache.imageFor(key: cacheKey) {
            completion(cacheImage, nil)
        } else {
            self.downloadImage(type: type, completion: { image, error in
                if let image = image {
                    type.cache.setImage(image: image, key: cacheKey)
                    completion(image, nil)
                } else {
                    completion(nil, error)
                }
            })
        }
    }
    
    func downloadAdditionalDataIfNeeded(usingMockService: Bool = false) {
        if !isAdditionalDataDownloaded {
            let requestData = TPDocumentData(id: identificator)
            let detailsService = usingMockService ? TPFakeDocumentDetailsService(requestData: nil)
                                                  : TPDocumentDetailsService(requestData: requestData)
            
            detailsService.request { [weak self] result in
                switch result {
                case .success(let result):
                    guard let document = result as? TPDocumentModel else {
                        return
                    }
                    
                    self?.documentModel = document
                    self?.isAdditionalDataDownloaded = true
                case .failure(let reason):
                    self?.showAlertClosure?(reason, Constants.Alerts.dataDownloading)
                }
            }
        }
    }
}

// MARK: Private methods
extension TPDocumentViewModel {
    private func retrieveImageFromCache(type: TPDocumentImageType) -> UIImage? {
        let cacheKey = String(identificator)
        
        return type.cache.imageFor(key: cacheKey)
    }
    
    private func removeHTMLTags(from text: String) -> String {
        return text.replacingOccurrences(of: "<p>", with: "", options: .literal, range: nil)
            .replacingOccurrences(of: "</p>", with: "\n\n", options: .literal, range: nil)
    }
    
    private func stringURL(of type: TPDocumentImageType) -> String? {
        switch type {
        case .Thumbnail:
            return thumbnailURL
        case .Header:
            return headerImageURL
        }
    }
    
    private func downloadImage(type: TPDocumentImageType, completion: @escaping (UIImage?, Error?) -> Void) {
        DispatchQueue.global().async {
            guard let stringUrl = self.stringURL(of: type),
                let url = URL(string: stringUrl) else {
                    completion(nil, nil)
                    return
            }
            
            let urlRequest = URLRequest(url: url)
            
            self.imageDownloader.download(urlRequest) { response in
                DispatchQueue.main.async {
                    if let image = response.result.value {
                        completion(image, nil)
                    } else {
                        completion(nil , response.error)
                    }
                }
            }
        }
    }
}
