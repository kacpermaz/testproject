//
//  TPListViewModel.swift
//  TestProject
//
//  Created by mazurkk3 on 02/11/2017.
//  Copyright © 2017 Kacper Mazurkiewicz. All rights reserved.
//

typealias TPAlertClosure = (String, String) -> Void

import Foundation

class TPListViewModel {
    
    var showAlertClosure : TPAlertClosure?
    var didReloadModelClosure : (() -> Void)?
    
    private var cellViewModels : [TPDocumentViewModel] = [TPDocumentViewModel]() {
        didSet {
            DispatchQueue.global().async {
                let sortedKeys = Array(Set(self.cellViewModels.map({ $0.category })).sorted(by: { $0 < $1 }))
                DispatchQueue.main.async {
                    self.sectionKeys = sortedKeys
                }
            }
        }
    }
    
    private var sectionKeys : [String] = [] {
        didSet {
            didReloadModelClosure?()
        }
    }
    
    var numberOfDocuments: Int {
        return cellViewModels.count
    }
    
    var numberOfSections: Int {
        return sectionKeys.count
    }
    
    func title(for section: Int) -> String {
        return sectionKeys[section]
    }
    
    func numberOfCells(forSection section: Int) -> Int {
        return cellViewModels.filter({ $0.category == sectionKeys[section] }).count
    }
    
    func getCellModel(at indexPath: IndexPath) -> TPDocumentViewModel {
        return cellViewModels.filter({ $0.category == sectionKeys[indexPath.section] })[indexPath.row]
    }
}

extension TPListViewModel {
    
    func downloadData(usingMockService: Bool = false) {
        let documentsService = usingMockService ? TPFakeDocumentsService(requestData: nil) : TPDocumentsService(requestData: nil)
                
        documentsService.request { [weak self] result in
            switch result {
            case .success(let result):
                guard let documents = result as? [TPDocumentModel] else {
                    return
                }
                DispatchQueue.global().async {
                    let viewModels = documents.map({ TPDocumentViewModel(model: $0) })
                    DispatchQueue.main.async {
                        self?.cellViewModels = viewModels
                    }
                }
            case .failure(let reason):
                self?.showAlertClosure?(reason, Constants.Alerts.dataDownloading)
            }
        }
    }
}
